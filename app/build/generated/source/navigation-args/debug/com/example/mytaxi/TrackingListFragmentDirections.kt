package com.example.mytaxi

import androidx.navigation.ActionOnlyNavDirections
import androidx.navigation.NavDirections

public class TrackingListFragmentDirections private constructor() {
  public companion object {
    public fun listToInfo(): NavDirections = ActionOnlyNavDirections(R.id.listToInfo)
  }
}
