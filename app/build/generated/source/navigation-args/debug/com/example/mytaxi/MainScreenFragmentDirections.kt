package com.example.mytaxi

import androidx.navigation.ActionOnlyNavDirections
import androidx.navigation.NavDirections

public class MainScreenFragmentDirections private constructor() {
  public companion object {
    public fun mainToInfo(): NavDirections = ActionOnlyNavDirections(R.id.mainToInfo)
  }
}
