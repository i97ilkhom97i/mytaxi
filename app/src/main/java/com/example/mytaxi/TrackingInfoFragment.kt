package com.example.mytaxi

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.mytaxi.databinding.FragmentTrackingInfoBinding
import com.google.android.material.bottomsheet.BottomSheetDialog


class TrackingInfoFragment : Fragment(R.layout.fragment_tracking_info) {
    private var _binding: FragmentTrackingInfoBinding? = null
    private val binding get() = _binding!!
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _binding = FragmentTrackingInfoBinding.bind(view)
        showBottomSheetDialog()

        binding.back.setOnClickListener { findNavController().popBackStack() }
    }

    private fun showBottomSheetDialog() {
        val dialog = BottomSheetDialog(requireContext())
        dialog.setContentView(
            LayoutInflater.from(requireContext()).inflate(R.layout.bottom_sheet, null, false)
        )
        dialog.create()
        dialog.show()
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}