package com.example.mytaxi

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.mytaxi.databinding.FragmentInfoBinding

class TrackingListFragment : Fragment(R.layout.fragment_info) {
    private val adapter = DateInfoAdapter()
    private var _binding: FragmentInfoBinding ?= null
    private val binding get() = _binding!!
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _binding = FragmentInfoBinding.bind(view)
        binding.recycle.adapter = adapter
        adapter.subMitList(getList())
        binding.recycle.layoutManager = LinearLayoutManager(requireContext())

        binding.back.setOnClickListener { findNavController().popBackStack() }

        adapter.setOnclickItem {
            findNavController().navigate(TrackingListFragmentDirections.listToInfo())
        }
    }

    private fun getList() : ArrayList<MainInfoData>{
        val ls = ArrayList<MainInfoData>()

        ls.add(MainInfoData.DateInfo("Iyul 7, Chorshanba"))
        ls.add(MainInfoData.TrackInfo("12.10", "15 000","Hadra ganga rassini 1","Yakkasaroy gai glinka 1"))
        ls.add(MainInfoData.TrackInfo("12.10", "15 000","Hadra ganga rassini 3","Yakkasaroy gai glinka 2"))
        ls.add(MainInfoData.DateInfo("Iyul 5, Dushanba"))
        ls.add(MainInfoData.TrackInfo("12.10", "15 000","Hadra ganga rassini 2","Yakkasaroy gai glinka 3"))
        ls.add(MainInfoData.TrackInfo("12.10", "15 000","Hadra ganga rassini 5","Yakkasaroy gai glinka 5"))
        ls.add(MainInfoData.TrackInfo("12.10", "15 000","Hadra ganga rassini 7","Yakkasaroy gai glinka 5"))
        ls.add(MainInfoData.DateInfo("Iyul 3, Sahnba"))
        ls.add(MainInfoData.TrackInfo("12.10", "15 000","Hadra ganga rassini 6","Yakkasaroy gai glinka 4"))
        ls.add(MainInfoData.TrackInfo("12.10", "15 000","Hadra ganga rassini 9","Yakkasaroy gai glinka 2"))
        ls.add(MainInfoData.TrackInfo("12.10", "15 000","Hadra ganga rassini 8","Yakkasaroy gai glinka 1"))
        return ls
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}