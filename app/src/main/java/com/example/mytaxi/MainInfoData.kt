package com.example.mytaxi

import android.os.Parcelable
import java.io.Serializable

sealed class MainInfoData : Serializable{
    data class TrackInfo(val date:String, val cost:String, val where:String, val to:String) : MainInfoData()
    data class DateInfo(val date:String) : MainInfoData()
}
