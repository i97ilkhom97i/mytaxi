package com.example.mytaxi

import android.location.Location
import android.location.LocationManager
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.core.view.GravityCompat
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.mytaxi.databinding.FragmentMainBinding
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng

import com.google.android.gms.maps.model.MarkerOptions


class MainScreenFragment : Fragment(R.layout.fragment_main), OnMapReadyCallback {
    private var _binding: FragmentMainBinding? = null
    private var locationManager: LocationManager? = null
    private var map: SupportMapFragment? = null
    private val binding get() = _binding!!
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _binding = FragmentMainBinding.bind(view)
        map =
            requireActivity().supportFragmentManager.findFragmentById(R.id.map) as? SupportMapFragment
        map?.getMapAsync(this)

        binding.includeFragment.menu.setOnClickListener {
            binding.drawerLayout.openDrawer(GravityCompat.START)
        }
        binding.navigation.setNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.myTrip -> {
                    findNavController().navigate(MainScreenFragmentDirections.mainToInfo())
                }
                else -> {
                    Toast.makeText(requireContext(), "Hali progressda", Toast.LENGTH_SHORT).show()
                }
            }
            return@setNavigationItemSelectedListener true
        }
    }

    override fun onMapReady(googleMap: GoogleMap) {
        googleMap.addMarker(
            MarkerOptions()
                .position(LatLng(41.32729547927393, 69.24767011527847))
                .title("Marker")
        )
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    private fun setCurrentLocation(location: Location) {

    }
}