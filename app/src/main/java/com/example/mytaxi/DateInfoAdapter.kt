package com.example.mytaxi

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import androidx.viewbinding.ViewBinding
import com.example.mytaxi.databinding.ItemDateBinding
import com.example.mytaxi.databinding.ItemInfoBinding
import java.lang.IllegalArgumentException

class DateInfoAdapter : RecyclerView.Adapter<DateInfoAdapter.VH>() {

    private var listenerClick : ((MainInfoData.TrackInfo) -> Unit) ?= null
    private var _bindingDate: ItemDateBinding? = null
    private var _bindingInfo: ItemInfoBinding? = null

    private val bindingDate get() = _bindingDate!!
    private val bindingInfo get() = _bindingInfo!!

    private val dateList = ArrayList<MainInfoData>()

    inner class VH(view: View) : RecyclerView.ViewHolder(view) {
        fun bind() {
            itemView.apply {
                if (itemViewType == MyConstants.DATE_ITEM){
                    _bindingDate = ItemDateBinding.bind(this)
                }
                else{
                    _bindingInfo = ItemInfoBinding.bind(this)
                }
                when (val data = dateList[adapterPosition]) {
                    is MainInfoData.TrackInfo -> {
                        bindingInfo.from.text = data.where
                        bindingInfo.to.text = data.to
                        bindingInfo.cost.text = data.cost
                        bindingInfo.mainBox.setOnClickListener { listenerClick?.invoke(data) }
                    }

                    is MainInfoData.DateInfo ->{
                        bindingDate.date.text = data.date
                    }
                }
            }
        }
    }

    fun subMitList(ls: ArrayList<MainInfoData>) {
        dateList.clear()
        dateList.addAll(ls)
        notifyItemRangeInserted(dateList.size, ls.size)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        val layout = when (viewType) {
            MyConstants.DATE_ITEM -> LayoutInflater.from(parent.context).inflate(R.layout.item_date, parent, false)
            MyConstants.TRACK_ITEM -> LayoutInflater.from(parent.context).inflate(R.layout.item_info, parent, false)
            else -> throw IllegalArgumentException("Errors appeared")
        }

        return VH(layout)
    }

    override fun onBindViewHolder(holder: VH, position: Int) = holder.bind()

    override fun getItemCount(): Int = dateList.size

    override fun getItemViewType(position: Int): Int {
        return when(dateList[position]){
            is MainInfoData.DateInfo -> MyConstants.DATE_ITEM
            is MainInfoData.TrackInfo -> MyConstants.TRACK_ITEM
        }
    }

    fun setOnclickItem(f: (MainInfoData.TrackInfo) -> Unit){
        listenerClick = f
    }
}
